<?PHP

//Log out
if($_GET[action] == "logout")
{
	session_start();
	session_destroy();
}

session_start();
 
require_once("header.html"); //Add title and page init

if($_SESSION[rpx])
{
	$auth = $_SESSION[rpx];
	
	require_once("./includes/init.inc.php"); //Do the initialization stuff
    
    $user =  new rpxuser($auth[identifier], $db, $auth); //Create user
   
    //set session
   	$_SESSION[iduser] = $user->getID(); //Set user session ID, for ajax api

	require_once("index_lib.php"); //Run library page
}
else
{
	require_once("./includes/init.inc.php"); //Do the initialization stuff
	require_once("index_login.php"); //Login page / Landing page
}

require_once("footer.html"); //Add common html (google tracker)
	
?>