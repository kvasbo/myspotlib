<?php

require_once("./includes/init.inc.php");

session_start();

if(!$_SESSION[iduser]){
	$tmp[alive] = 0;
	outputJson(json_encode($tmp));
	die();
}

$userid = $_SESSION[iduser];

//Create debug object
$debug = array();

switch ($_GET[action])
{
	case "ping":
		$tmp[alive] = 1;
		outputJson($tmp);
		break;
	case "getalbum":
		outputJson(getAlbumInfo($_GET[url]));
		break;
	case "getalbumsforuser":
		outputJson(getAlbumsForUser());
		break;
	case "getalbumsbyartist":
		outputJson(getAlbumsByArtist());
		break;
	case "getartistsforuser":
		outputJson(getArtistsForUser());
		break;
	case "addalbumtouser":
		outputJson(addAlbumToUser($_GET[url]));
		break;
	case "removealbumfromuser":
		outputJson(removeAlbumFromUser($_GET[id]));
		break;
	case "gettracks":
		outputJson(getTracks($_GET[id]));
		break;
	case "getsettings":
		$user = new rpxuser($userid, $db, null, true);
		outputJson($user->settings);
		break;
	case "storesettings":
		outputJson(storeSettings($_GET[settings]));
		break;
}

//Single point of output.
function outputJson($dataarray)
{
	//$out = $dataarray;
	$out[payload] = $dataarray;
	$out[debugstatus] = "1";
	$out[debug] = "Debug data";
	$out[action] = $_GET[action];
	
	echo json_encode($out);
}

//Persist user settings to database
function storeSettings($settings)
{

	global $userid;
	global $db;
	
	//Decode
	$settings = base64_decode($settings);
	$settings = json_decode($settings);
	
	//Read, with some very stupid input protection.
	$view_covers = substr($settings->view_covers, 0, 4);
	$view_controls =  substr($settings->view_controls, 0, 4);
	$view_last =  substr($settings->view_last, 0, 4);
	
	//Store
	$query = "UPDATE user_settings SET view_covers = '$view_covers', view_controls = '$view_controls', view_last = '$view_last' WHERE id_user = '$userid'";
	
	$db->query($query);
	
	$return[result] = 1;
	$return[query] = $query;
	
	return $return;
	
}

function getAlbumInfo($url)
{
	global $spotify;
	
	$id = base64_decode($_GET[url]);
	
	$data = $spotify->getAlbumData($id);
	
	if($data)
	{	
		$output = $data;
	}
	
	return $output;	
}

function addAlbumToUser($url)
{
	global $userid;
	global $spotify;
	global $db;
	global $user;
	
	$url = base64_decode($_GET[url]);
	
	$data = $spotify->getAlbumData($url);
	
	$query = "UPDATE user_has_album SET active = '1' WHERE id_user = '".$userid."' AND id_album = '".$data[id_album]."'";
	
	$db->query($query);
	
	if($db->getAffected() == 0){
	
		$query = "INSERT INTO user_has_album(id_user, id_album, id_artist, name, artist, released) VALUES('".$userid."','".$data[id_album]."','".$data[id_artist]."','".base64_decode($data[name])."','".base64_decode($data[artist])."','".$data[year]."')";
		$db->query($query);
		$return[q] = base64_encode($query);
		$return[result] = 1;
	}
	else
	{
		$return[result] = 1;
		$return[query] = "undeleted";
	}
	
	return $return;
	
}

function removeAlbumFromUser($id)
{
	global $userid;
	global $spotify;
	global $db;
	global $user;
	
	$id = base64_decode($id);
	
	$query = "UPDATE user_has_album SET active = '0' WHERE id_user = '".$userid."' AND id_album = '".$id."'";
	$db->query($query);
	
	$result[result] = 1; 
	$result[q] = 1;
	
	return $result;
	
}

function getAlbumsForUser()
{	
	global $userid;
	global $db;
	global $user;
	global $spotify;
	
	//$query = "SELECT album.*, artist.name as 'artist' FROM album, artist WHERE album.id_album IN (SELECT id_album FROM user_has_album WHERE id_user = '".$userid."' AND active = '1') AND album.id_artist LIKE artist.id_artist AND album.id_artist NOT LIKE '' GROUP BY album.id_album ORDER BY album.name ASC";
	
	$query = "SELECT * FROM user_has_album WHERE id_user = '".$userid."' AND name NOT LIKE '' AND active = '1' ORDER BY name";
	
	$results = $db->query($query);
	
	while($album = $results->fetch_assoc())
	{	
		$album[name] = base64_encode(utf8_encode($album[name]));
		$album[artist] = base64_encode(utf8_encode($album[artist]));		
			
		$albums[] = $album;
	}
	
	if($albums)
	{
		$output = $albums;
	}
	
	return $output;
	
}

function getAlbumsByArtist()
{
	global $userid;
	global $db;
	//global $user;
	//global $spotify;
	
	$query = "SELECT * FROM user_has_album WHERE id_user = '".$userid."' AND name NOT LIKE '' AND active = '1' ORDER BY artist ASC, name ASC";
	
	$results = $db->query($query);
	
	while($album = $results->fetch_assoc())
	{	
		$album[name] = base64_encode(utf8_encode($album[name]));
		$album[artist] = base64_encode(utf8_encode($album[artist]));		
			
		$albums[] = $album;
	}
	
	if($albums)
	{
		$output = $albums;
	}
	
	return $output;
	

}

function getArtistsForUser()
{
	global $db;
	global $user;
	global $userid;
	
	//$query = "SELECT a.* FROM artist a, album al WHERE al.id_album IN (SELECT id_album FROM user_has_album WHERE id_user = '".$userid."' AND active = '1') AND a.id_artist LIKE al.id_artist GROUP BY a.id_artist ORDER BY name ASC";
	
	$query = "SELECT id_artist, artist FROM user_has_album WHERE id_user = '".$userid."' AND artist NOT LIKE '' AND active = '1' GROUP BY artist ORDER BY artist";
	
	$results = $db->query($query);
	
	while($artist = $results->fetch_assoc())
	{	
		$artist[name] = base64_encode(utf8_encode($artist[artist]));
		$artists[] = $artist;
	}
	
	if($artists)
	{
		$output = $artists;
	}
	
	return $output;
	
}

function getTracks($id)
{

}

?>