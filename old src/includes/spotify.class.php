<?PHP

//Spotify communication, needs cache!

class Spotify
{

	private $db;
	
	public function __construct($database){
		$this->db = $database;
		$this->curl_last;
		
	}
	
	//Connect to Spotify, get album data.
	function getAlbumData($url, $force = false)
	{
		
		//Check for valid url
		if($this->checkURL($url))
		{
		
		//Get data from url
		$urlInfo = $this->analyze($url);
		
		//Try to read album from cache
		$cacheData = $this->getAlbumFromCache($urlInfo[id]);
		
		//If we have cache data, return them, else get API data. If force API; go API antwatyas
		if($cacheData || $force)
		{
			$tracks = $this->getAlbumTracks($urlInfo[id]);
			$cacheData[tracks] = $tracks;
			return $cacheData;
		}
		else
		{
			//Get data from Spotify API
			$data = $this->doAlbumApiCall($urlInfo[id]);
			
			//Convert to standard object
			$datao = simplexml_load_string($data[data]);
			
			//Read out data
			$info[id_album] = $urlInfo[id];
			$info[year] = (string) $datao->released;
			$info[id_spotify] = (string) $datao->id;
			$info[id_artist] = (string) substr($datao->artist[href],-22);
			$info[name] = base64_encode((string) $datao->name);
			$info[artist] = base64_encode((string) $datao->artist->name);
			
			//Get all other albums by artist
			$this->getAlbumsByArtist($info[id_artist]); //Find all albums by artist. Should check for cache.
			
			//Add debug data
			$info[debug][curl] = $data[debug_curl];
			
			//Go thhrough tracks and add to cache
			foreach($datao->tracks->track as $track)
			{
				//Stupid dashes in xml
				$tracktag = "track-number";
				$disctag = "disc-number";
				
				//Get track data
				 $tmp[name] = (string) $track->name;
				 $tmp[disc] = (string) $track->$disctag;
				 $tmp[track] = (string) $track->$tracktag;
				 $tmp[time] = (string) $track->length;
				 $tmp[id_track] = substr((string) $track[href],-22);
				 
				 //Add track to database
				 new Track($tmp,$info[id_album],$info[id_artist]);
			}
			
			//Retrieve the tracks
			$info[tracks] = $this->getAlbumTracks($urlInfo[id]);
			
			return $info;
			
		}
		
		}
		else{
			return false;
		}
	}
	
	//Get album data from cache
	private function getAlbumFromCache($idalbum)
	{
	
		global $db;
		$query = "SELECT album.*, artist.name as 'artist' FROM album, artist WHERE album.id_artist LIKE artist.id_artist AND album.id_artist NOT LIKE '' AND album.id_album LIKE '".$idalbum."'";
		
		$results = $db->query($query);
		
		if($results->num_rows == 1)
		{
			$data = $results->fetch_assoc();
			$info[id_album] = $data[id_album];
			$info[year] = $data[released];
			$info[id_spotify] = $data[id_spotify];
			$info[id_artist] = $data[id_artist];
			$info[name] = base64_encode($data[name]);
			$info[artist] = base64_encode($data[artist]);
			
			$info[debug][curl] = "0";
			$info[debug][cached] = "1";
			
			return $info;
		}
		else
		{
			return false;
		}
		
	}
	
	
	//Get album tracks, from cache or system
	private function getAlbumTracks($idalbum)
	{
		global $db;
		
		$query = "SELECT * FROM track WHERE id_album LIKE '".$idalbum."'";
		$result = $db->getAll($query);
		
		if(count($result) > 0)
		{
		
			foreach($result as $track)
			{
				unset($tmp);
			
				$tmp[id_track] = $track[id_track];
				$tmp[disc] = $track[disc];
				$tmp[time] = $track[time];
				$tmp[name] = base64_encode($track[name]);
				$tmp[track] = $track[number];
			
				$tracks[] = $tmp;
			}
		}
		else //NO tracks in db
		{
			//$tmp = $this->getAlbumData("spotify:album:".$idalbum, true);
			//$tracks = $tmp[tracks];
		}
		
		return $tracks;
						
	}
	
	//Get all albums by artist, and store both artist and albums in database. 
	private function getAlbumsByArtist($artistid)
	{
		
		$data = $this->doArtistApiCall($artistid);
		
				
		$datao = simplexml_load_string($data[data]);
		
		
		
		foreach($datao->albums->album as $album)
		{
			
			$datatmp[id_album] = substr((string) $album[href],-22);
			$datatmp[id_artist] = substr((string) $album->artist[href],-22);
			$datatmp[id_spotify] = (string) $album->id;
			$datatmp[name] = (string) $album->name;
			$datatmp[released] = (string) $album->released;
			$datatmp[artist_name] = (string) $album->artist->name;
			
			new Album($datatmp);
			new Artist($datatmp);
			
		}

		
	}
		
	private function checkURL($url)
	{
		//check for spotify url
		if(strtolower(substr($url,0,8)) == "spotify:")
		{
			return "spotify";
		}
		elseif(strtolower(substr($url,0,24)) == "http://open.spotify.com/")
		{
			return "http";
		}
		elseif(strtolower(substr($url,0,17)) == "open.spotify.com/")
		{
			return "http";
		}
		else
		{
			return false;
		}

	}
	
	//Find url category
	private function analyze($url)
	{
		if(strpos($url,"album")) $info[type] = "album";
		//elseif(strpos($url,"artist")) $info[type] = "artist";
		//elseif(strpos($url,"track")) $info[type] =  "track";
		else $info[type] = false; //Unknown type. Dammit.
		
		$info[id] = substr($url,-22);
		
		return $info;
		
	}
	
	//Get artist data from API
	private function doAlbumApiCall($albumid)
	{
		if(strlen($albumid) == 22)
		{
			$getURL = "http://ws.spotify.com/lookup/1/?uri=spotify:album:".$albumid."&extras=trackdetail";
			return $this->doApiCall($getURL);
		}
		else
		{
			return false;
		}
	}
	
	//Get album data from API
	private function doArtistApiCall($artistid)
	{
		if(strlen($artistid) == 22)
		{
		$getURL = "http://ws.spotify.com/lookup/1/?uri=spotify:artist:".$artistid."&extras=albumdetail";
		return $this->doApiCall($getURL);
		}
		else
		{
			return false;
		}
	}
	
	//Perform an api call to the Spotify server. No API calls must ever go outside this function due to limits.
	private function doApiCall($url)
	{
	
		// create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

		$info = curl_getinfo($ch);

        // close curl resource to free up system resources
        curl_close($ch);  
        
        //Log the request
       	if($info[http_code] != "200")
       	{
        	$query = "INSERT INTO apilog(url, response) VALUES('".$url."','".$info[http_code]."')";
        	$this->db->query($query);
        	$return[debug_curl] = $info;
        	   
		}
		else
		{
			
			$return[debug_curl] = $info;
		}
		
		$return[data] = $output;
		

		return $return;
		
	}	
	
}


?>