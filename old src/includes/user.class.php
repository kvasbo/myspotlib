<?PHP


class rpxuser
	{
		
		private $userid;
		private $fbuser;
		//private $facebookid;
		private $db;
		private $token;
		
		public function __construct($fbid, $database, $fbdata = null, $useinternalid = false)
		{
			//$this->facebookid = $fbid;
			$this->db = $database;
			
			if(!$useinternalid) $tempdata = $this->getOrCreateUser($fbid, $fbdata);
			else $tempdata = $this->getUser($fbid);
			
			$this->id = $tempdata[id];
			$this->fbuser = $tempdata[fbuser];
			
			$this->firstname = $tempdata[fname];
			$this->lastname = $tempdata[lname];
			$this->token = $this->createToken();
			
			$this->settings = $this->getSettings();
						
		}
		
		public function createToken()
		{
			$query = "UPDATE users SET timestamp = NOW() WHERE id_user LIKE '".$this->id."'";
			$this->db->query($query);
			
			$query = "SELECT timestamp FROM users WHERE id_user LIKE '".$this->id."'";
			
			$timestamp = $this->db->getOne($query);
			$timestamp = $timestamp[result][timestamp];

			$token = md5($this->fbuser.$timestamp);
			
			$query = "INSERT INTO user_token(id_user, token, timestamp) VALUES('".$this->id."','".$token."','".$timestamp."')";
			$this->db->query($query);
			
		}
		
		public function getToken()
		{
			return $this->token;
		}
	
	
		private function getUser($id)
		{
			$query = "SELECT * FROM users WHERE id_user = '$id' LIMIT 1";
			
			$result = $this->db->query($query);
			
			$tmp = mysqli_fetch_assoc($result);
			
			$data[id] = $tmp[id_user];
			$data[fname] = $tmp[firstname];
			$data[lname] = $tmp[lastname];
			$data[fbuser] = $tmp[id_facebook];
			
		}
		
		private function getOrCreateUser($fbid,$fbdata){
			
			$query = "SELECT * FROM users WHERE id_facebook = '$fbid' LIMIT 1";
			
			$result = $this->db->query($query);
			
			
			//Create user in database
			if($result->num_rows == 0)
			{
				$data[id] = $this->addNewUser("fb", $fbdata);
				
				$data[fname] = $fbdata[name][givenName];
				$data[lname] = $fbdata[name][familyName];
				$data[fbuser] = $fbid;				
			}
			else
			{
				$tmp = mysqli_fetch_assoc($result);
				
				$data[id] = $tmp[id_user];
				$data[fname] = $tmp[firstname];
				$data[lname] = $tmp[lastname];
				$data[fbuser] = $tmp[id_facebook];
			}
			
			return $data;
			
		}
		
		private function addNewUser($type, $fb)
		{
		
			global $db;
			
			if($type = "fb") //Facebook user
			{	
				$id = $fb[identifier];
				$fname = $fb[name][givenName];
				$lname = $fb[name][familyName];
				$prov = $fb[providerName];
				$email = $fb[verifiedEmail];

				$query = "INSERT INTO users(id_facebook, firstname, lastname, provider, email) VALUES('$id', '$fname','$lname', '$prov', '$email')";
				
			//	echo $query;
				
				$this->db->query($query);
				$id = $db->getLastID();
			
				
				return $id;
			}
			
		}
		
		//Get settings for a suser
		public function getSettings()
		{
			$query = "SELECT * FROM user_settings WHERE id_user = '$this->id' LIMIT 1";
			
			$result = $this->db->query($query);
			
			//Create default user settings in database
			if($result->num_rows == 0)
			{
				$query2 = "INSERT INTO user_settings(id_user) VALUES ('$this->id')";
				$result2 = $this->db->query($query2);				
			
				$result = $this->db->query($query);
			}
			
			$tmp = mysqli_fetch_assoc($result);
				
			$data[view_covers] = $tmp[view_covers];
			$data[view_last] = $tmp[view_last];
			$data[view_controls] = $tmp[view_controls];

			return $data;	
			
		}
		
		public function getID()
		{
			return $this->id;
		}
		
		public function getFBId()
		{
			return $this->fbid;
		}
		
		public function getName()
		{
			return $this->firstname . " " . $this->lastname;
		}
		
		
	}



?>