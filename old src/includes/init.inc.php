<?php

$include_dir = "includes/";


require_once($include_dir."database.class.php");
require_once($include_dir."user.class.php");

require_once($include_dir."spotify.class.php");
require_once($include_dir."album.class.php");
require_once($include_dir."artist.class.php");
require_once($include_dir."track.class.php");

//External libraries
require_once($include_dir."simple_html_dom.php");

$db = new db();
$spotify = new Spotify($db);

?>