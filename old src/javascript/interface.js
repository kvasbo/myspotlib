//Toggle lookup area 
function toggleControlArea(){
	
	//Get cookie
	var showing = Get_Cookie("controlarea");
	
	//Toggle showing of control area
	if(showing == "show")
	{
		Set_Cookie("controlarea", "hide");
		settings.view_controls = 0;
	}
	else {
		Set_Cookie("controlarea", "show");
		settings.view_controls = 1;
	}
	
	storeAjaxSettings();
	
	controlArea();
		
}

//Toggle lookup area 
function toggleViewType(){
	
	setNumberOfTiles(8);
	
	//Get cookie
	var showing = Get_Cookie("viewtype");
	
	//Toggle showing of control area
	if(showing == "list")
	{
		Set_Cookie("viewtype", "cover");
	}
	else if(showing == "cover")
	{
		Set_Cookie("viewtype", "list");
	}
	else {
		Set_Cookie("viewtype", "list");
	}
		
	setView();
		
}


function controlArea(){
	
	var showing = Get_Cookie("controlarea");
	
	if(showing == "hide")
	{
		$('#controlarea').slideUp();
		//$("#showhidecontrols").html("Show controls");
	}
	else
	{
		$('#controlarea').slideDown();
		//$("#showhidecontrols").html("Hide controls");
	}
	
}

function setView(){
	
	var view = Get_Cookie("viewtype");
	
	if(view == "list")
	{
		$("#s_wrap").hide();
		$("#bar1").show();
		$("#bar2").show();
		//$("#toggleview").html("Cover view");
	}
	else
	{
		$("#s_wrap").show();
		$("#bar1").hide();
		$("#bar2").hide();
		//$("#toggleview").html("List view");
	}
}

//Change size of covers in cover view
function setNumberOfTiles(tiles)
{

	tiles = (+tiles);
	var nTiles = 8;
	
	var prevTiles  = Get_Cookie("tiles");
	
	if(prevTiles == 6)
	{
		if(tiles == -1) nTiles = 6;
		else nTiles = 8;
	}
	else if(prevTiles == 8)
	{
		if(tiles == -1) nTiles = 6;
		else nTiles = 10;
	}
	else if(prevTiles == 10)
	{
		if(tiles == -1) nTiles = 8;
		else nTiles = 10;
	}
	
	Set_Cookie("tiles", nTiles);
	
	//usermessage("Showing "+nTiles+" tiles", "Interface");
	
	switch(nTiles){
  		 		
  		case 6:
  			$(".s_album").height(158).width(158);
  			$(".s_album img").height(156).width(156);
  			$(".s_info").width(152); 
  		break;
  		
		case 8:
  			$(".s_album").height(116).width(116);
  			$(".s_album img").height(114).width(114);
  			$(".s_info").width(110);
  		break;
  
  		case 10:
  			$(".s_album").height(91).width(91);
  			$(".s_album img").height(89).width(89);
  			$(".s_info").width(86);
  		break;

	}
}

function hoverOverCover(id)
{
	var el = document.getElementById("si_"+id);
	$(el).fadeIn();
}

function noHoverOverCover(id)
{
	var el = document.getElementById("si_"+id);
	$(el).fadeOut();
}

function modalsetup(){
	
	$(".modalTrigger").overlay({
	top: '30%',
		mask: {

		// you might also consider a "transparent" color for the mask
		color: '#fff',

		// load mask a little faster
		loadSpeed: 200,

		// very transparent
		opacity: 0.6
	},
	closeOnClick: false
	});
}