function checkAjaxConnection(){
	
	var getUrl = 'ajax.php?action=ping';

	$.getJSON(getUrl, receiveAjax);
}

function setAjaxAlive(value)
{
	ajaxAlive = value;
}

function receiveAjax(data)
{
	//Check debug status in package
	if(data.debugstatus != "1")
	{
		avlus(data.debug);
	}

	//Run appropiate action
	if(data.action == "getartistsforuser")
	{
		populateArtists(data.payload);
	}
	else if(data.action == "getalbumsforuser")
	{
		RecordCollection = setRecordCollection(data.payload);
		populateRecordList(RecordCollection);
		usermessage("Collection loaded, "+RecordCollection.length+" records", "Init");
	}
	else if(data.action == "ping")
	{
		setAjaxStatus(data.payload);
	}
	else if(data.action == "getcoverurl")
	{
		updateAlbumCoverImage(data.payload);
	}
	else if(data.action == "getalbumsbyartist")
	{
		RecordsByArtist = setRecordCollection(data.payload);
		populateCovers(RecordsByArtist);
	}
	else if(data.action == "getsettings")
	{
		settings = data.payload;
		usermessage("Settings loaded","Init");
		console.log(settings);		
	}
	else if(data.action == "storesettings")
	{	
		console.log("store");
		console.log(data);
	}
	

}

function setAjaxStatus(data)
{
	if(data.alive == "1"){
					 
 			setAjaxAlive(true);
 	 }  
}

//Load settings from server
function getAjaxSettings(){
	
	var getUrl = "/ajax.php?action=getsettings";
	$.getJSON(getUrl, receiveAjax);
	
}

function storeAjaxSettings(){

	var settingString = Base64.encode(JSON.stringify(settings));
	var getUrl ="/ajax.php?action=storesettings&settings="+settingString;
	console.log(getUrl);
	$.getJSON(getUrl, receiveAjax);

}

function populateArtistsFromAjax()
{
		var getUrl = "/ajax.php?action=getartistsforuser";
		
		$.getJSON(getUrl, receiveAjax);			
}

function getRecordCollectionFromAjax()
{
		var getUrl = "/ajax.php?action=getalbumsforuser";
		
		$.getJSON(getUrl, receiveAjax);
		
		var getUrl = "/ajax.php?action=getalbumsbyartist";
		
		$.getJSON(getUrl, receiveAjax);
			
}

//Add any album to library, url is verified spotify url (but this is also checked on the server)
function addAlbum(url)
{
	var b64 = Base64.encode(url);
	var getUrl = 'ajax.php?action=addalbumtouser&url='+b64;
	avlus("Opening "+getUrl);
	
	$.getJSON(getUrl, function(data) {
 		 usermessage("Album added", "Info");
 	     refresh();
 		 currenturl = ""; //Set last successful URL to zero, we have now added.
		});
}

function albumAdded(data)
{
	 usermessage("Album added", "Info");
 	 refresh();
     currenturl = ""; //Set last successful URL to zero, we have now added.
}

//Check out an url
function somethingDropped(url)
	{
		
		var b64 = Base64.encode(url);
		var getUrl = 'ajax.php?action=getalbum&url='+b64;
		
		avlus("Opening "+getUrl);
		
		$.getJSON(getUrl, function(data) {
 		 
 		 usermessage(Base64.decode(data.payload.name) + " found", "Info");
 		 
 		 currenturl = url; //Set last successful URL to currenturl
 		 
 		 if(autoadd == true) //Add last album if that is what we do
 		 {
 		 	addCurrentAlbum(); 
 		 }
 		 
		});
}	


function setRecordCollection(data)
{
	
	var tmpColl = [];
	
	for (var i=0; i < data.length; i++) 
		{
			
			var temp = [];
			temp[0] = data[i].id_album;
			temp[1] = Base64.decode(data[i].name);
			temp[2] = Base64.decode(data[i].artist);
			temp[3] = data[i].id_artist;
			temp[4] = data[i].released;
				
			tmpColl[i] = temp;
			     		
		}
			
	return tmpColl;
}

