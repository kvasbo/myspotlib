
//Set up collection
var debugon = false;
var RecordCollection = []; //Holds entire collection
var RecordsByArtist = [];
var lastcontent = ""; //last content seen
var test1 = "spotify:album:"; //first possible
var test2 = "http://open.spotify.com/album/"; //second possible
var currenturl = "";
var autoadd = false;
var ajaxAlive = false;
var initAttempts = 0;
var coverDivs = [];
var lastArtist = ""; //LAst artist rendered, for use in determining whether artist name needs showing
var settings;

function init()
{

	initAttempts = initAttempts + 1;

	controlArea(); //Sustain control area state
	setView(); //Set correct view
	autoAddToggle(); //Take care of remembered form states
	checkAjaxConnection(); //Test ajax 
	modalsetup();
	
	//Create menu ribbon
	$('#flyout-ribbon').FlyoutRibbon();
  	
	

	if(ajaxAlive){
	
		usermessage("Connection verified ("+initAttempts+")", "Init");
		
		getAjaxSettings();
		
		
	
		populateArtistsFromAjax(); //Populate artists

		getRecordCollectionFromAjax(); //Get existing records
	
	
		checkDropbox(); //Start the check thingy
		
	}
	else
	{
		if(initAttempts > 10)
		{
			window.location.reload(); //Last resort, do new login
		}
		else
		{
			var t=setTimeout("init()", 500); //Loopy loop
		}
	}
	
	Set_Cookie("token", "<?PHP echo 'test'; ?>");
	
}

function refresh()
{
	populateArtistsFromAjax(); //Populate artists

	getRecordCollectionFromAjax(); //Get existing records
	

}

//Add last successful album to library
function addCurrentAlbum()
{
	if(currenturl != ""){
		usermessage("Adding "+currenturl+" to library", "Info");
		addAlbum(currenturl);
	} 
	else {
		usermessage("Nothing to add to library", "Error");
	}
}

function removeAlbum(idalbum)
{
	var b64 = Base64.encode(idalbum);
	var getUrl = 'ajax.php?action=removealbumfromuser&id='+b64;
	
		$.getJSON(getUrl, function(data) {
 		 usermessage("Album removed", "Info");
 	     refresh();
		});
}



//Toggle auto add funciton
function autoAddToggle()
{
    autoadd = $("#autoadd").attr('checked');
	
	if(autoadd)
	{
		//usermessage("Auto add enabled", "info");
	}
	else {
		//usermessage("Auto add disabled", "info");
	}
}

//Check paste area
function checkDropbox()
{	

	var tmpMessage = "";

	var text = document.getElementById("dropbox").value;
	
	if(text != lastcontent) //Only check when there is a change
	{
		lastcontent = text; //reset last text
		
		avlus("content changed, checking "+text);
		
		
		
	if(text.length == 36 || text.length == 52) // Right size?
	{
		
		
		if(text.substr(0,14) == test1 || text.substr(0,30) == test2) //and the correct text?
		{
			
			avlus("found url! checking "+text);
			
			tmpMessage = "Spotify URL identified, fetching data";
			
			currenturl = "";
			
			somethingDropped(text);
			
			clearContent();
			
		}
		else {
			clearContent();
			currenturl = "";
			tmpMessage = "'"+text + "' is not a Spotify album URL", "error";
		}
	
	}
	else
	{
		clearContent();
		currenturl = "";
		tmpMessage = "'"+text + "' is not a Spotify album URL", "error";
	}
	
	}
	
	if(tmpMessage != "") usermessage(tmpMessage, "Info"); //Output last user message
	
	var t=setTimeout("checkDropbox()", 200); //Loopy loop
}

//clear the dropbox
function clearContent(){
	avlus("Emptying paste box"); //Just a debug
	document.getElementById("dropbox").value = ""; //Empty the box
	lastcontent = ""; //Reset last content to stop refire of check-thingy
}


	


function populateArtists(data)
{
	var artistlist = "<li onclick='artistSelectedLi(\"all\")' class='listbutton'>Show albums by all artists</li>";

		for (var i=0; i < data.length; i++) 
		{
		
		 	var name = Base64.decode(data[i].name);
		 	
		 	artistlist += "<li id='"+data[i].id_artist+"' onclick='artistSelectedLi(\""+data[i].id_artist+"\")'>" + name + "</li>\n";
	 	
		 		
		}
			
	$("#artistlist").html(artistlist);
}



//Called when artist is clicked
function artistSelectedLi(id)
{

	filterAlbumsOnArtistChange(id);
	//usermessage(ajaxAlive);
}



//Filters the list
function filterAlbumsOnArtistChange(id)
{
	
	var list = $("#albumlist").children();

	
	if(id == "all")
	{
		for(var i = 0; i <list.length; i++)
		{
			if($(list[i]).hasClass("album")){
				$(list[i]).fadeIn();
			}
		}
	}
	else
	{

		for(var i = 0; i <list.length; i++)
		{

			var albumid = list[i].id.substr(23,22);
				
			var selector = "#"+list[i].id;
				
			if(albumid == id)
			{
				$(list[i]).fadeIn();
			}
			else
			{

				$(list[i]).hide();
				
			}
			
		}
		
	}
	
}

function albumSelected(id)
{
	//Open spotify url
	var url = "spotify:album:"+id;
	location.href = url;
}



//Populate record list with all albums
function populateRecordList(collection)
{

	$('#albumlist').html("");
		
	for(var i = 0; i < collection.length; i++)
	{
	
		var album = collection[i][0];
		var artist = collection[i][3];
		var name = collection[i][1];
		var artname = collection[i][2];
		var year = collection[i][4];
		
		addAlbumToList(album,artist,name,artname,year);	//Adds the album and the tooltip				
	}
	
}

function populateCovers(collection)
{
	$('#s_albums').html("");
		
	for(var i = 0; i < collection.length; i++)
	{
	
		var album = collection[i][0];
		var artist = collection[i][3];
		var name = collection[i][1];
		var artname = collection[i][2];
		var year = collection[i][4];
		
		addAlbumToScroll(album,artist,name,artname,year);	//Adds the album and the tooltip
				
	}
}

//Cover screen, add album
function addAlbumToScroll(album,artist,name,artname,year)
{

	var bigId = album + "." + artist+"." + Math.random(1);
	
	var str="<div id='s_"+bigId+"' class='s_album'><img src='http://myspotlib.com/covers/"+album+".jpg' onmouseover='hoverOverCover(\""+bigId+"\")' onmouseout='noHoverOverCover(\""+bigId+"\")'  onclick='albumSelected(\""+album+"\")'>";
	
	str += "<div id='si_"+bigId+"' class='s_info'>"+artname+"<br/>"+name+"</div>";
	
	$("#s_albums").append(str);
	
	var justAdded = document.getElementById("s_"+bigId);
	
	
}

//Build an album element
function addAlbumToList(album,artist,name,artname,year)
{
	var bigId = album + "." + artist;
	
	var str = "<li id='"+bigId+"' onclick='albumSelected(\""+album+"\")' class='album' >" + name + "</li>\n";
	
    str += "<!-- tooltip element --><div class='tooltip'><div class='s_artist' style='display:none'>"+artname+"</div><div class='s_album' style='display:none'>"+name+"</div><div class='cover' id='c."+bigId+"'><img src='http://myspotlib.com/covers/"+album+".jpg'></div><div class='title'>"+artname+"<br/><b>"+name+"</b><br>"+year+"</br></div><div class='controls'><a href='#' onclick='albumSelected(\""+album+"\")'>Open</a> - <a href='#' onClick='removeAlbum(\""+album+"\")'>Remove</a></div></div>";
	
	$('#albumlist').append(str);
	
	var justAdded = document.getElementById(bigId);
	
	
	$(justAdded).tooltip(
	
	{ position: "center left",
	  opacity: 1,
	  predelay: 500,
	  delay: 100,
	  effect: "fade",

	}
	
	);

}


//Filter in cover view 
function coverFilter()
{
	var value = document.getElementById("coverFilter").value;
	
	value = cleanStupidHTMLChars(value);

	avlus(value);
	
	var list = $("#s_albums").children();
	
		
	for(var i = 0; i <list.length; i++)
	{

		if($(list[i]).hasClass("s_album")){

			var artist = list[i].innerHTML;
			artist = cleanStupidHTMLChars(artist);
			
			var found = artist.search(value);

			
			if(found != -1)
			{
				if($(list[i]).hasClass("s_album")) $(list[i]).fadeIn(); //Show if its an album
			}
			else
			{	
				$(list[i]).hide();
			}
		
		}
		
	}
	
}


//Filter on album title
function albumFilter()
{
	var value = document.getElementById("albumFilter").value;
	
	
	value = cleanStupidHTMLChars(value);

	avlus(value);
	
	var list = $("#albumlist").children();
	
		
	for(var i = 0; i <list.length; i++)
	{

		if($(list[i]).hasClass("album")){

			var artist = list[i].innerHTML;
			artist = cleanStupidHTMLChars(artist);
			
			var found = artist.search(value);

			
			if(found != -1)
			{
				if($(list[i]).hasClass("album")) $(list[i]).fadeIn(); //Show if its an album
			}
			else
			{	
				$(list[i]).hide();
			}
		
		}
		
	}
	
}

//Filter on artist
function artistFilter()
{
	var value = document.getElementById("artistFilter").value;
	value = cleanStupidHTMLChars(value);
	
	var list = $("#artistlist").children();
	
	for(var i = 1; i <list.length; i++)
	{

		var artist = list[i].innerHTML;
		artist = cleanStupidHTMLChars(artist);
		
		
		
		var found = artist.search(value);
		
		if(found != -1)
		{
			$(list[i]).fadeIn();
		}
		else
		{	
			$(list[i]).hide();
		}
		
	}
	
}

//Clean out stupid html entities, for filters and search.
function cleanStupidHTMLChars(s) {
 	
 	s = s.toString();
 	
 	//Remove all html entities
 	s = s.replace(/\&[a-z0-9A-Z]+\;/g, "");
 	
 	//lower case the entire thing
 	s = s.toLowerCase();
 	
 	//Remove non-letters or numbers
 	s = s.replace(/[^a-z0-9 ]/g, "");
 	
 	return s;
}

function usermessage(text, type)
{
	$("#result").html(type+": "+text+"<br>"+$("#result").html());
}

function avlus(text)
{
	if(debugon == true)
	{
		$("#debug").html(text.toString() +"<br>"+$("#debug").html());
	}	
}