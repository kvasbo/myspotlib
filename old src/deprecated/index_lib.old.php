<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >


<head>
<title>MySpotLib</title>

<link rel="stylesheet" type="text/css" href="styles/reset-fonts-grids.css">
<link rel="stylesheet" type="text/css" href="styles/base.css">
<link rel="stylesheet" type="text/css" href="styles/style.css">



<script src="http://www.google.com/jsapi" type="text/javascript"> </script>
<script src="javascript/base64.js" type="text/javascript"> </script>

<script type="text/javascript">
  
  google.load("jquery", "1.4.2");
  google.load("jqueryui", "1.8.1");

</script>


<script src="javascript/functions.js" type="text/javascript"> </script>
<script src="javascript/javascript_cookies.js" type="text/javascript"> </script>


</head>

<body onLoad="init()">

<div id="doc4" class="yui-t7">
	<div id="hd">
	
	<div id="header">
		<span onclick="$('#controlarea').slideToggle();">Hide/show controls</span>
	</div> <!-- header end -->
	
	<div id="controlarea">
	
	<div id="dropper">

	<form>
		<input type="text" name="dropbox" id="dropbox">
	</form>

	</div> <!-- dropper end -->
	

	<div id="result">
		&nbsp;
	</div> 
	
	
	<div id="addbutton" onclick="addCurrentAlbum()">
		Add album
	</div>
	
	<div id="autochoose">
		<input type="checkbox" value="auto" id="autoadd" onchange="autoAddToggle()">&nbsp;&nbsp;Auto add
	</div>
	
	
	</div> <!-- controlarea end -->
	
	</div> <!-- hd end -->
	
	<div id="bd">
		<div id="yui-main">
			<div class="yui-b">
				<div class="yui-gb">
					<div class="yui-u first">
						
						<div id="artists">
							<h1>Artists</h1>
							<input type="text" id="artistFilter" onkeyup="artistFilter()">
							<div class="listcontainer">
							<ul id="artistlist" class="listbox" >
							
							</ul>
							</div>
							
						</div>
					</div>
					<div class="yui-u">
						<div id="albums">
							<h1>Albums</h1>
							<input type="text" id="albumFilter" onkeyup="albumFilter()">
							<div class="listcontainer">
							<ul id="albumlist" class="listbox">
							
							</ul>
							</div>
						
						</div>
					</div>
					<div class="yui-u">
						<div id="albums2">
							<h1>Albums</h1>
							<input type="text" id="albumFilter2" onkeyup="albumFilter()">
							<div class="listcontainer">
							<div id="albumlist2" class="listbox">
							
							</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="yui-b">
			<!-- PUT SECONDARY COLUMN CODE HERE -->
		</div>
	</div>
	<div id="ft">
		<div id="debug">
		</div>
	</div>
</div>

</body>

</html>