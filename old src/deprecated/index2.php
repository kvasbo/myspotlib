<?PHP

require_once("header.html");

if($cookie)
{	
	require_once("./includes/init.inc.php"); //Do the initialization stuff
    $user =  new user($cookie['uid'], $db, $fbuser); //Create user
    
    //set session
    session_start();
	$_SESSION[iduser] = $user->getID(); //Set user session ID, for ajax api
    
	require_once("index_lib.php"); //Run library page
}
else
{
	require_once("index_login2.php"); //Login page / Landing page
}

//print_r($fbuser);
	
?>