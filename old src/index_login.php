<?PHP 

session_start(); 
$lastfive = $db->getAll("SELECT id_album FROM user_has_album ORDER BY id_relation DESC LIMIT 5");

?>

<!-- My styles -->
<link rel="stylesheet" type="text/css" href="styles/structure.css">
<link rel="stylesheet" type="text/css" href="styles/style.css">

 </head>
  <body>
  
  <div id="wrap">
  
  <div id="header">
  
  </div>
  
  <div id="bigbox">
  
 
  
  	<table>
  	<tr><td>What?</td><td>A page that lets you recreate (or just create) your record collection in Spotify</td></tr>
  	<tr><td>Why?</td><td>Because the library function in Spotify does not work</td></tr>
  	<tr><td>How?</td><td><ol><li>Log in with your existing account from one of the providers listed below</li><li>Drag and drop an album title (album url) from Spotify to your page on mySpotLib, and the album is automagically added to your online record collection</li><li>Click the album cover or title to open it in the Spotify Client.</li></ol></td></tr>
  	</table>
  
  </div>
  
  <center>
      <iframe src="http://myspotlib.rpxnow.com/openid/embed?token_url=http%3A%2F%2Fmyspotlib.com%2Frpx.php"  scrolling="no"  frameBorder="no"  allowtransparency="true"  style="width:400px;height:240px"></iframe> 
  </center>  
  
  <p>&nbsp;</p>
    
    <script type="text/javascript">
  var rpxJsHost = (("https:" == document.location.protocol) ? "https://" : "http://static.");
  document.write(unescape("%3Cscript src='" + rpxJsHost +
"rpxnow.com/js/lib/rpx.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
  RPXNOW.overlay = true;
  RPXNOW.language_preference = 'en';
</script>

<div id="lastfive">
<center>Last five albums added: <br>
	<?PHP
	
	while($album = each($lastfive))
	{
		echo "<img src='http://myspotlib.com/covers/";
		echo $album[value][id_album];
		echo ".jpg' />";
	}
	
	?>
</center>
</div>


</div>