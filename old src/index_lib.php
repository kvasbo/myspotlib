<!-- My styles -->
<link rel="stylesheet" type="text/css" href="styles/structure.css">
<link rel="stylesheet" type="text/css" href="styles/style.css">

<link rel="stylesheet" type="text/css" href="styles/flyout.ribbon.css">

<link rel="stylesheet" type="text/css" href="styles/contextmenu.css">

<!-- My functions -->
<script src="javascript/functions.js" type="text/javascript"> </script>
<script src="javascript/ajax.js" type="text/javascript"> </script>
<script src="javascript/interface.js" type="text/javascript"> </script>

<script src="javascript/jquery.contextmenu.js" type="text/javascript"> </script>

<!-- Cookie functionality -->
<script src="javascript/javascript_cookies.js" type="text/javascript"> </script>

</head>

<body onLoad="init()">

<div id="wrap">

<div id="nav">
	
	<div id="controlarea">
	
	<div id="dropper">

	<form>
		<input type="text" name="dropbox" id="dropbox">
	</form>

	</div> <!-- dropper end -->
	

	<div id="result">
		&nbsp;
	</div> 
	
	
	<div id="addbutton" onclick="addCurrentAlbum()">
		Add album
	</div>
	
	<div id="autochoose">
		<input type="checkbox" value="auto" id="autoadd" onchange="autoAddToggle()" checked="">&nbsp;&nbsp;Auto add
	</div>
</div>

<div id="s_wrap" style="display: none">

<div>

<span onclick="setNumberOfTiles(-1)">-</span>
<span onclick="setNumberOfTiles(1)">+</span>

<span>Filter: <input type="text" id="coverFilter" onkeyup="coverFilter()"></span></div>
<div id="s_albums" >

</div>
</div>


<div id="bar1" class="mainbox">
<div id="artists">
<h1>Artists</h1>
<center><span>Filter: <input type="text" id="artistFilter" onkeyup="artistFilter()"></span></center>
<div class="listcontainer">
<ul id="artistlist" class="listbox" >
<li>Downloading artist list...</li>
</ul>
</div>

</div>
</div>

<div id="bar2" class="mainbox">
<div id="albums">
<h1>Albums</h1>
<center><span>Filter: </span><input type="text" id="albumFilter" onkeyup="albumFilter()"></span></center>
<div class="listcontainer">
<ul id="albumlist" class="listbox">
<li>Downloading album list...</li>
</ul>
</div>

</div>
</div>

<div id="footer">
		<div id="debug">
		
		</div>
</div>

</div>

<!-- Dialog boxes and such, not displayed on page -->

<div id="flyout-ribbon" class="ribbon">
  <ul>
    <li><a href="#" id="logout" href="?action=logout"><img src="icons1/user-delete.png" alt="Log out" width="55" height="55" /><br>Log out</a></li>
    <li><a onclick="toggleViewType()" id="toggleview"  href="#" ><img src="icons1/laptop.png" alt="" width="55" height="55" /><br>Switch view</a></li>
       <li><a onclick="toggleControlArea()" id="showhidecontrols"  href="#" ><img src="icons1/pencil.png" alt="" width="55" height="55" /><br>Edit</a></li>
  </ul>
</div>
